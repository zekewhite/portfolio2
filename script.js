function scrollToContent() {
    const contentContainer = document.getElementById('contentContainer');
    contentContainer.style.opacity = 1;

    const sections = document.querySelectorAll('.container section');
    sections.forEach((section) => {
        section.style.opacity = 1;
    });

    window.scrollTo({
        top: contentContainer.offsetTop,
        behavior: 'smooth'
    });
}
